
import { createApp } from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
// import { faPhone, faPlus, faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from '@/store'

library.add(
    // faUserSecret,
    // faPhone,
    // faPlus,
    far,
    fas
    )

const app = createApp(App) 
app.config.productionTip = false;

app.use(store)
app.use(router)
    .component('font-awesome-icon', FontAwesomeIcon)
app.mount('#app')

// import 'bootstrap/dist/js/bootstrap'