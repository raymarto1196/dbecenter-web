import { LoginOutlined } from '@ant-design/icons-vue/lib/icons'
import { createRouter, createWebHashHistory } from 'vue-router'
import Navigation from "../components/Navigation.vue"
import Footer from '@/components/footer/Footer.vue'
import Home from '../views/web/home/Home.vue'
import AuthLayout from '../views/authentication/LoginView.vue'
import UserDashboard from '../views/dashboard/DashboardView.vue'



const routes = [
  {
    path: '/',
    redirect: '/home',
  },

  {
    path:'/home',
    name:'Home',
    components:{
      default: Home,
      navigation: Navigation,
      footer: Footer
    } 
  },

  {
    path: '/dbe-certified-businesses',
    name: 'dbe-certified-businesses',
    components:{
      default:  () => import('../views/web/domore/certified/CertifiedBusiness.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },

  {
    path: '/project-owners-gcs-agencies',
    name: 'project-owners-gcs-agencies',
  
    components:{
     default: () => import('../views/web/domore/projowners/ProjectOwners.vue'),
     navigation: Navigation,
     footer: Footer
    }
  },

  {
    path: '/capability-statement',
    name: 'capability-statement',
    components:{  
      default: () => import('../views/web/domore/capability/CapabilityStatement.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },

  {
    path: '/eotr-series',
    name: 'eotr-series',
    components:{  
      default: () => import('../views/web/resources/eotr/EotrView.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },

  {
    path: '/fab-working-capital-loan',
    name: 'fab-working-capital-loan',
    components:{  
      default: () => import('../views/web/resources/fab/FabView.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },

  {
    path: '/partners',
    name: 'partners',
    components:{  
      default: () => import('../views/web/resources/partners/PartnersView.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },

  {
    path: '/shopzone',
    name: 'shopzone',
    components:{  
      default: () => import('../views/web/shopzone/ShopZone.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },  

  {
    path: '/shopzone/products',
    name: 'products',
    components:{  
      default: () => import('../views/web/shopzone/products/ShopZoneProduct.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },

  {
    path: '/shopzone/services',
    name: 'services',
    components:{  
      default: () => import('../views/web/shopzone/services/ShopZoneServices.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },
  
  {
    path: '/shopzone/projects',
    name: 'project',
    components:{  
      default: () => import('../views/web/shopzone/projects/ShopZoneProject.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },
  
  {
    path: '/shopzone/projects/full-details/:item_id',
    name: 'project-full-details',
    components:{  
      default: () => import('../views/web/shopzone/projects/ShopZoneFullProjectDetails.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },
  
  {
    path: '/shopzone/services/full-details/:item_id',
    name: 'service-full-details',
    components:{  
      default: () => import('../views/web/shopzone/services/ShopZoneFullServiceDetails.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },
  
  {
    path: '/shopzone/products/full-details/:item_id',
    name: 'product-full-details',
    components:{  
      default: () => import('../views/web/shopzone/products/ShopZoneFullProductDetails.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },
  
  {
    path: '/shopzone/search_results/:search_string',
    name: 'search-results',
    components:{  
      default: () => import('../views/web/shopzonesearchresult/ShopZoneSearchResult.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },
  
  {
    path: '/shopzone/user/full-details/:id',
    name: 'user-full-details',
    components:{  
      default: () => import('../views/web/shopzonesearchresult/UserFullDetails.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },

  {
    path: '/investors',
    name: 'investors',
    components:{  
      default: () => import('../views/web/investors/Investors.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },

  {
    path: '/contact-us',
    name: 'contact-us',
    components:{  
      default: () => import('../views/web/contact/Contact.vue'),
      navigation: Navigation,
      footer: Footer
    }
  },
    
  {
    path:'/login',
    component: AuthLayout
  },

  {
    path:'/user-dashboard',
    name:'user-dashboard',
    components: {
      default: () => import('../views/dashboard/DashboardView.vue'),
      navigation: Navigation,
      footer: Footer
    },  
  },

]

const router = createRouter({
  history: createWebHashHistory('/'),
  routes
})

export default router
