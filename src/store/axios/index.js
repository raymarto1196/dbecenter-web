import axios from 'axios'
require('dotenv').config();
// console.log(process.env) 

const api = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
})

api.interceptors.request.use(function (config) {
    const token = localStorage.getItem('blog_token');
    if(token) {
        config.headers.Authorization =  `Bearer ${token}`;
        return config;
    }
});



export default api