

import { createStore } from 'vuex'

import USERAUTH from '@/views/authentication/index';
import DASHBOARD from '@/views/dashboard/index';
import SHOPZONE from '@/views/web/shopzone/index'
import CART from '@/views/cart/index'
// import AUTH from './auth/index'


export const store = createStore({

  modules: {
    USERAUTH,
    DASHBOARD,
    SHOPZONE,
    CART
}
})
export default store