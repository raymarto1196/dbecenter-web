
import axios from "axios";

const DEFAULT_STATE = () => ({
    error: {
        message     :'',
        check       : false,
        message2    :'',
        check2      : false
    },
    auth:{
        isLoggedin  : false,
        auth        : false,
      }
})

const ACTIONS = {
    async loginUser(ctx, user){ 
    const response = await axios.post(process.env.VUE_APP_BASE_URL+'/api/v1/auth/login', {
        auth        : true,
        login       : user.email,
        password    : user.password
    }).then(response => { 
        console.log('success login')
        localStorage.setItem("blog_token", response.data.accessToken,)
        localStorage.setItem("user_id", response.data.user.id)
        localStorage.setItem("username", response.data.user.username)
        localStorage.setItem("user_role", response.data.user.roles[0].name)
        localStorage.setItem("isLoggedin", true)
        window.location.replace("/#/user-dashboard")
    })
    .catch(error => {
        ctx.commit('commit_error', error.response) 
    })
    return response
    },
    async signupUser(ctx, user) {
        const response = await axios.post(process.env.VUE_APP_BASE_URL+'/api/v1/auth/register', {
            firstname               : user.firstname,
            lastname                : user.lastname,
            username                : user.username,
            email                   : user.email,
            role                    : user.role,
            type                    : user.type,    
            password                : user.password,
            password_confirmation   : user.password_confirmation
        }).then(response => { 
            localStorage.setItem("blog_token", response.data.accessToken)
            localStorage.setItem("user_id", response.data.user.id)
            localStorage.setItem("username", response.data.user.username)
            localStorage.setItem("isLoggedin", true)
            window.location.replace("/#/user-dashboard")
        })
        .catch(error => {
            ctx.commit('commit_error_registration', error.response) 
        })
        return response
    },
    async logoutUser(){ 
        localStorage.removeItem('blog_token')
        localStorage.removeItem('user_id')
        localStorage.removeItem('isLoggedin')
        window.location.replace("/#/home")
        window.location.reload("/#/home")
    }
}
const MUTATIONS = {
    commit_error(state, data){
        // state.error.message = data.data.message;
        state.error.check = true;
        console.log(data);
    },commit_error_registration(state, data){
        // state.error.message2 = data.data.message;
        state.error.check2 = true;
    },
}

const GETTERS = {}

const USERAUTH = {
    namespaced: true,
    state: DEFAULT_STATE(),
    actions: ACTIONS,
    mutations: MUTATIONS,
    getters: GETTERS
}

export default USERAUTH