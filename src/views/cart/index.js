
import axios from "axios";
import api from '@/store/axios/index'

const DEFAULT_STATE = () => ({
    userCart: [],
    userCartList: [],
    searchItemIdentifierList: [],
    searchCompanyNaicsList: [],
    searchNaicCodeList: [],
    searchNaicCodeDescList: [],
    searchNaicCodeandDescList: [],
    searchServiceList: [],
    searchProductList: [],
    searchProjectList: [],
    searchCompanyList: [],
})

const ACTIONS = {  
    reloadPage(){
        window.location.reload()
    },
    async getUserCart(ctx){
        const response = await api.get('/api/v1/user/cart')
        .then(response => { 
        ctx.commit('userCart', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async AddToCart(ctx, data){
        const response = await api.post('/api/v1/carts',{
            item_id  : data.id,
            quantity : data.quantity
        })
        .then(response => { 
            window.location.reload()
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async DeleteItemFromCart(ctx, id){
        const response = await api.delete('/api/v1/carts/'+id)
        .then(response => { 
            window.location.reload()
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },  
    // async noParamsNaicsCode(ctx){
    //     const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/naics_code')
    //     .then(response => { 
    //         ctx.commit('searchNaicCodeList', response.data) 
    //     })
    //     .catch(error => {
    //         console.log(error)
    //     })
    //     return response
    // },      
    async getNaicsCode(ctx, data){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/search?search_string='+data)
        .then(response => { 
            ctx.commit('searchNaicCodeList', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },    
    async getSearchItemList(ctx){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/itemList')
        .then(response => { 
            ctx.commit('searchItemList', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async getSearchCompanyList(ctx, search_string){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/user/companies')
        .then(response => { 
            ctx.commit('searchCompaniesItemList', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async searchServiceWithParams(ctx, data){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/items/category/1?search_string='+data)
        .then(response => { 
            ctx.commit('searchServiceList', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async searchProductWithParams(ctx, data){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/items/category/2?search_string='+data)
        .then(response => { 
            ctx.commit('searchProductList', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },  
    async searchprojectWithParams(ctx, data){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/items/category/3?search_string='+data)
        .then(response => { 
            ctx.commit('searchProjectList', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },          
    async searchCompanyWithParams(ctx, data){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/user/companies?search_string='+data)
        // console.log('results2', response)
        .then(response => { 
            ctx.commit('searchCompanyList', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },        
    async placeOrderApi(ctx, data){
        let arr = [];
        if( data.items){
            data.items.forEach(element => {
            let item = {
                    cart_item_id     : element.id,
                    item_id          : element.item_id,
                    quantity_ordered : element.quantity
                }
                arr.push(item);
             });
        }
        const response = await api.post('/api/v1/orders',{
            name             : data.name,
            telephone        : data.telephone,
            description      : data.description,
            shipping_address : data.street+', '+data.city+', '+data.region+', '+data.zipcode+', '+data.country,
            items            : arr,
        })
        .then(response => { 
            window.location.reload()
        })
        .catch(error => {
            console.log(error)
        })
        return response
    }
}
const MUTATIONS = {
    userCart(state, items){
        if(items){
            state.userCart = items.results.cart
            let arr = [];
            if( items.results.cart){
                items.results.cart.cart_items.forEach(element => {
                    arr.push(element);
                 });
            }
            state.userCartList = arr
        }
    },
    searchNaicCodeList(state, items){
        let arr = [];
        let arr2 = [];
        let arr3 = [];
        items.results.forEach(element => {
            arr.push(element.code);
            arr2.push(element.description );
            arr3.push(element.code +' - '+ element.description );
        });
        state.searchNaicCodeList = arr
        state.searchNaicCodeDescList = arr2
        state.searchNaicCodeandDescList = arr3
    },      
    searchItemList(state, items){
        let arr = [];
        items.results.forEach(element => {
            arr.push(element.identifier);
        });
        state.searchItemIdentifierList = arr
    },    
    searchServiceList(state, items){
        let arr = [];
        items.results.forEach(element => {
                arr.push(element);
        });
        state.searchServiceList = arr
    },    
    searchProductList(state, items){
        let arr = [];
        items.results.forEach(element => {
                arr.push(element);
        });
        state.searchProductList = arr
    },    
    searchProjectList(state, items){
        let arr = [];
        items.results.forEach(element => {
                arr.push(element);
        });
        state.searchProjectList = arr
    },
    searchCompanyList(state, companies){
        let arr = [];
        companies.results.forEach(element => {
                arr.push(element);
        });
        state.searchCompanyList = arr
    },
    searchCompaniesItemList(state, companies){
        let arr = [];
        companies.results.forEach(element => {
           let data = arr.includes(element)
             element.naics_codes.forEach(element => {
                arr.push(element);
            })
        });
        state.searchCompanyNaicsList = arr
    }
}

const GETTERS = {}

const CART = {
    namespaced: true,
    state: DEFAULT_STATE(),
    actions: ACTIONS,
    mutations: MUTATIONS,
    getters: GETTERS
}

export default CART