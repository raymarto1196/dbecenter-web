
import axios from "axios";
import api from '@/store/axios/index'

const DEFAULT_STATE = () => ({
    imageUrl : process.env.VUE_APP_IMAGE_FOLDER_URL,
    user: { //sample data
        id              : localStorage.getItem('user_id'),
        profile_image   : '',
        username        : '',
        email           : '',
        type            : '',
        firstname       : '',
        middlename      : '',
        lastname        : '',
        telephone       : '',
        address1        : '',
        address2        : '',
        city            : '',
        region          : '',
        country         : '',
        zipcode         : '',
        role            : '',
        company         : {
                            name            : '',
                            logo            : '',
                            description     : '',
                            telephone       : '',
                            address1        : '',
                            address2        : '',
                            city            : '',
                            region          : '',
                            zipcode         : '',
                            country         : '',
                            duns_number     : '',
                            cage_code       : '',
                            naics_codes     : [],
                            differentiators : [],
                            capabilities    : [],
                            references      : [],
                            certifications  : [],
                            government_designation  : [],
                         }
    },
    userItemList          : [],
    userServices          : [],
    userProducts          : [],
    productFullDetails    : [],
    userOrders            : [],
    userCustomerOrders    : [],
    vendorPorjectBids     : [],
    vendorAvailServices     : [],
    contractorAvailedServices : [],
    companyAssessment : [],
    userFullDetails : [],
    setSpecificItem : { 
        id : '',
        identifier : '',
        item_description : '',
        item_name : '',
        item_image : '',
        item_type : '',
        category_id : '',
        quantity : '',
        unit_cost : '',
        star_rating : 0 
    },
})

const ACTIONS = {
    async getUserData(ctx){
        const response = await api.get('/api/v1/user')
        .then(response => { 
            ctx.commit('commit_user', response.data) 
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async UpdateUser(ctx, data){
        const response = await api.put('/api/v1/user/profile',{
            username                : data.username,   
            email                   : data.email,      
            type                    : data.type,       
            firstname               : data.firstname, 
            middlename              : data.middlename,
            profile_picture         : data.profile_picture, 
            lastname                : data.lastname, 
            telephone               : data.telephone, 
            address1                : data.address1,  
            address2                : data.address2,  
            city                    : data.city,    
            region                  : data.region,   
            country                 : data.country,  
            zipcode                 : data.zipcode,  
            role                    : data.role,       
            password                : data.password,      
            password_confirmation   : data.password_confirmation       
        })
        // console.log(response)
        .then(response => {
            ctx.dispatch('getUserData');
          }        
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async UpdateCompany(ctx, data){
        const response = await api.put('/api/v1/user/company',{
            name        : data.name,   
            logo        : data.logo,   
            description : data.description, 
            telephone   : data.telephone, 
            address1    : data.address1,  
            address2    : data.address2,  
            city        : data.city,    
            region      : data.region,   
            country     : data.country,  
            zipcode     : data.zipcode,  
            naics_codes : data.naics_codes,       
            duns_number : data.duns_number,       
            cage_code   : data.cage_code,      
        })
        .then(response => {
              ctx.dispatch('getUserData');
            }
        )
        .catch(error => {
            console.log(error)
        })
        return response

    },
    async UpdateCertificate(ctx, data){
        const response = await api.put('/api/v1/user/company/certifications',{
            certifications : data,   
        })
        .then(response => {
                ctx.dispatch('getUserData');
            }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },       
    async updateGovDesig(ctx, data){
        console.log(data)
        const response = await api.put('/api/v1/user/company/government-designation',{
            government_designation : data,   
        })
        .then(response => {
                ctx.dispatch('getUserData');
            }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },    
    async UpdateDifferentiator(ctx, data){
        const response = await api.put('/api/v1/user/company/differentiators',{
            differentiators : data,   
        })
        .then(response => {
                ctx.dispatch('getUserData');
              }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async UpdateCapabilities(ctx, data){
        const response = await api.put('/api/v1/user/company/capabilities',{
            capabilities : data,   
        })
        .then(response => {
                ctx.dispatch('getUserData');
              }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async UpdateReferences(ctx, data){
        const response = await api.put('/api/v1/user/company/references',{
            references : data,   
        })
        .then(response => {
                ctx.dispatch('getUserData');
              }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async getUserItemList(ctx, userid){
        const response = await api.get('/api/v1/users/'+userid+'/items')
        .then(response => {
            ctx.commit('userItems', response) 
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async getUserServices(ctx, category_id){
        const response = await api.get('/api/v1/user/items/category/'+category_id)
        .then(response => {
            ctx.commit('userServices', response) 
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async getUserProducts(ctx, category_id){
        const response = await api.get('/api/v1/user/items/category/'+category_id)
        .then(response => {
            ctx.commit('userProducts', response) 
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async getUserProjects(ctx, category_id){
        const response = await api.get('/api/v1/user/items/category/'+category_id)
        .then(response => {
            ctx.commit('userProjects', response) 
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async createItem(ctx, data){
        const response = await api.post('/api/v1/items', {
            identifier      : data.identifier,
            item_type       : data.item_type,
            item_image      : data.item_image,
            category_id     : data.category_id,
            item_name       : data.item_name,
            item_description: data.item_description,
            quantity        : data.quantity,
            unit_cost       : data.unit_cost,
            from            : data.from,
            to              : data.to
        })
        .then(response => {
            if(data.category_id == 1){
                 ctx.dispatch('getUserServices', 1);
            }else if(data.category_id == 2){
                 ctx.dispatch('getUserProducts', 2);
            }else if (data.category_id == 3){
                 ctx.dispatch('getUserProjects', 3);
            }
            return response
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async updateItem(ctx, data){
        const response = await api.put('/api/v1/items/'+data.id,{
            identifier      : data.identifier,
            item_type       : data.item_type,
            item_image      : data.item_image,
            category_id     : data.category_id,
            item_name       : data.item_name,
            item_description: data.item_description,
            quantity        : data.quantity,
            unit_cost       : data.unit_cost,
            from            : data.from,
            to              : data.to
        })
        .then(response => {
            if(data.category_id == 1){
                ctx.dispatch('getUserServices', 1);
            }else if(data.category_id == 2){
                ctx.dispatch('getUserProducts', 2);
            }else if (data.category_id == 3){
                ctx.dispatch('getUserProjects', 3);
            }
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async fetchItem(ctx, data){
        const response = await api.get('/api/v1/items/'+data.id)
        .then(response => {
            ctx.commit('setSpecificItem', response.data) 
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async destroyItem(ctx, data){
        const response = await api.delete('/api/v1/items/'+data.id)
        .then(response => {
            if(data.category_id == 1){
                ctx.dispatch('getUserServices', 1);
            }else if(data.category_id == 2){
                ctx.dispatch('getUserProducts', 2);
            }else if (data.category_id == 3){
                ctx.dispatch('getUserProjects', 3);
            }
          }
        )
        .catch(error => {
            console.log(error)
        })
    },
    async fetchOrder(ctx){
        const response = await api.get('/api/v1/user/orders')
        .then(response => {
            ctx.commit('setOrders', response.data.results) 
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async cancelOrder(ctx, id){
            const response = await api.put('/api/v1/user/order/status/'+id,{
            status : 'cancelled'
        })
        .then(response => {
            ctx.dispatch('fetchCustomerOrder');
            ctx.dispatch('fetchOrder');
          }
        )
        .catch(error => {
            console.log(error)
        })
    },
    async approvedOrder(ctx, id){
        const response = await api.put('/api/v1/user/order/status/'+id,{
            status : 'approved'
        })
        .then(response => {
            ctx.dispatch('fetchCustomerOrder');
          }
        )
        .catch(error => {
            console.log(error)
        })
    },
    async cancelAvailedService(ctx, id){
        const response = await api.put('/api/v1/user/availed_service/status/'+id,{
            status : 'cancelled'
        })
        .then(response => {
            ctx.dispatch('fetchAvailedService');
            ctx.dispatch('fetchContractorAvailedService');
          }
        )
        .catch(error => {
            console.log(error)
        })
    },
    async approvedAvailedService(ctx, id){
        const response = await api.put('/api/v1/user/availed_service/status/'+id,{
            status : 'approved'
        })
        .then(response => {
            ctx.dispatch('fetchAvailedService');
          }
        )
        .catch(error => {
            console.log(error)
        })
    },
    async fetchCustomerOrder(ctx){
        const response = await api.get('/api/v1/user/contractor_orders')
        .then(response => {
            ctx.commit('setCustomerOrders', response.data.results) 
          }
        )
        .catch(error => {
            console.log(error)
        })
        return response

    },
    async postRatingComment(ctx, data){
        const response = await api.post('/api/v1/ratings',{
            item_id : data.item_id,
            rate    : data.rate,
            comment : data.comment
        })
        .then(response => { 
            window.location.reload()
        })
        .catch(error => {
            console.log(error)
        })
        return response
    },
    async deleteRatingComment(ctx, id){
        const response = await api.delete('/api/v1/ratings/'+id)
        .then(response => {
            window.location.reload()
          }
        )
        .catch(error => {
            console.log(error)
        })
    },    
    async fetchVendorProjectBids(ctx, id){
        const response = await api.get('/api/v1/user/project_bids')
        .then(response => {
            ctx.commit('setVendorPorjectBids', response.data.results) 
          }
        )
        .catch(error => {
            console.log(error)
        })
    },
    async fetchAvailedService(ctx){
        const response = await api.get('/api/v1/user/vendor/availed_services')
        .then(response => {
            ctx.commit('setVendorAvailServices', response.data.results) 
          }
        )
        .catch(error => {
            console.log(error)
        })
    },   
    async fetchContractorAvailedService(ctx){
        const response = await api.get('/api/v1/user/contractor/availed_services')
        .then(response => {
            ctx.commit('setContractorAvailServices', response.data.results) 
          }
        )
        .catch(error => {
            console.log(error)
        })
    },        
    async updateCompanyAssessment(ctx, data){
        const response = await api.put('/api/v1/user/company/assessments',{
            assessment_results_list : data
        })
        .then(response => {
            ctx.dispatch('fetchCompanyAssessment');
          }
        )
        .catch(error => {
            console.log(error);
        })      
            return response;
    },
    async fetchCompanyAssessment(ctx){
        const response = await api.get('/api/v1/user/company/assessments/results')
        .then(response => {
            ctx.commit('setCompanyAssessment', response.data.results) 
          }
        )
        .catch(error => {
            console.log(error);
        })      
        
        return response;
    },
    // async fetchCompanyAssessment(ctx){
    //     const response = await api.get('/api/v1/user/company/assessments')
    //     .then(response => {
    //         ctx.commit('setCompanyAssessment', response.data.results) 
    //       }
    //     )
    //     .catch(error => {
    //         console.log(error)
    //     })
    // },    
    async getUserFullDetails(ctx,id){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/user/full_details/'+id)
        .then(response => {
            ctx.commit('userFullDetails', response.data.results) 
          }
        )
        .catch(error => {
            console.log(error)
        })
    },

}
const MUTATIONS = {
    commit_user(state, data) {
        state.user.username          = data.results.username
        state.user.email             = data.results.email
        state.user.type              = data.results.type
        state.user.firstname         = data.results.profile.firstname
        state.user.middlename        = data.results.profile.middlename
        state.user.lastname          = data.results.profile.lastname
        state.user.profile_picture   = data.results.profile.profile_picture
        state.user.telephone         = data.results.profile.telephone
        state.user.address1          = data.results.profile.address1
        state.user.address2          = data.results.profile.address2
        state.user.city              = data.results.profile.city
        state.user.region            = data.results.profile.region
        state.user.zipcode           = data.results.profile.zipcode
        state.user.country       = data.results.profile.country
        state.user.role          = data.results.roles[0].name
        if(data.results.company != null){
            state.user.company.name         = data.results.company.name
            state.user.company.logo         = data.results.company.logo
            state.user.company.description  = data.results.company.description
            state.user.company.telephone    = data.results.company.telephone
            state.user.company.address1     = data.results.company.address1
            state.user.company.address2     = data.results.company.address2
            state.user.company.city         = data.results.company.city
            state.user.company.region       = data.results.company.region
            state.user.company.zipcode      = data.results.company.zipcode
            state.user.company.country      = data.results.company.country
            state.user.company.duns_number  = data.results.company.duns_number
            state.user.company.cage_code    = data.results.company.cage_code
            state.user.company.naics_codes  = data.results.company.naics_codes
            state.user.company.differentiators  = data.results.company.differentiators
            state.user.company.capabilities     = data.results.company.capabilities
            state.user.company.references       = data.results.company.references
            state.user.company.certifications   = data.results.company.certifications
            state.user.company.government_designation   = data.results.company.government_designation
        }
    },
    userItems(state, items){
        let arr = [];
        items.data.results.forEach(element => {
                arr.push(element);
        });
        state.userItemList = arr
    },   
    setSpecificItem(state, data) {
        state.setSpecificItem.id               = data.results.id
        state.setSpecificItem.identifier       = data.results.identifier
        state.setSpecificItem.item_description = data.results.item_description
        state.setSpecificItem.item_name        = data.results.item_name
        state.setSpecificItem.item_image       = data.results.item_image
        state.setSpecificItem.item_type        = data.results.item_type
        state.setSpecificItem.category_id      = data.results.category_id
        state.setSpecificItem.unit_cost        = data.results.unit_cost
        state.setSpecificItem.from             = data.results.from
        state.setSpecificItem.to               = data.results.to
        state.setSpecificItem.status           = data.results.status
        
    },
    userServices(state, items){
        let arr = [];
        items.data.results.forEach(element => {
                arr.push(element);
        });
        state.userServices = arr
    },
    userProducts(state, items){
        // console.log('product',items)
        let arr = [];
        items.data.results.forEach(element => {
                arr.push(element);
        });
        state.userProducts = arr
    },
    userProjects(state, items){
        let arr = [];
        items.data.results.forEach(element => {
                arr.push(element);
        });
        state.userProjects = arr
    },
    setOrders(state,data){
        let arr = [];
        data.orders.forEach(element => {
                arr.push(element);
        });
        state.userOrders = arr

    },
    setCustomerOrders(state,data){
        let arr = [];
        data.forEach(element => {
                arr.push(element);
        });
        state.userCustomerOrders = arr

    },
    setVendorPorjectBids(state,data){
        let arr = [];
        data.forEach(element => {
                arr.push(element);
        });
        state.vendorPorjectBids = arr
    },
    setVendorAvailServices(state,data){
        let arr = [];
        data.forEach(element => {
                arr.push(element);
        });
        state.vendorAvailServices = arr
    },
    setContractorAvailServices(state,data){
        let arr = [];
        data.availed_services.forEach(element => {
                arr.push(element);
        });
        state.contractorAvailedServices = arr
    },      
    setCompanyAssessment(state,data){
        state.companyAssessment = data.company_assessment.assessment_results_list;
    },   
    userFullDetails(state,data){
        state.userFullDetails = data
    }
}

const GETTERS = {}

const DASHBOARD = {
    namespaced: true,
    state: DEFAULT_STATE(),
    actions: ACTIONS,
    mutations: MUTATIONS,
    getters: GETTERS
}

export default DASHBOARD