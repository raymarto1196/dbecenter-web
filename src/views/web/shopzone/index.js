
import axios from "axios";
import api from '@/store/axios/index'

const DEFAULT_STATE = () => ({
    imageUrl : process.env.VUE_APP_IMAGE_FOLDER_URL,
    error: {
        message:'',
        check : false
    },
    list_of_projects        : [],
    list_of_services        : [],
    list_of_product         : [],
    product_item_ratings    : [],
    project_bids            : [],
    setSpecificItem : { 
        id                  : '',
        identifier          : '',
        item_description    : '',
        item_name           : '',
        item_type           : '',
        category_id         : '',
        star_rating         : 0 
    },
    setFullDetailsItem : []
})

const ACTIONS = {  
    async getProjectList(ctx, user){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/items/category/3'
        ).then(response => { 
            ctx.commit('setProjectList', response) 
            // console.log('success List of Projects')
        })
        .catch(error => {
            console.log(error.response)
        })
    },  
    async getProductList(ctx, user){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/items/category/2'
        ).then(response => { 
            ctx.commit('setProductList', response) 
            // console.log('success List of Product')
        })
        .catch(error => {
            console.log(error.response)
        })
    },
    async getServiceList(ctx, user){
        const response = await axios.get(process.env.VUE_APP_BASE_URL+'/api/v1/items/category/1'
        ).then(response => { 
            ctx.commit('setServiceList', response) 
            // console.log('success List of Services')
        })
        .catch(error => {
            console.log(error.response)
        })
    },  
    async fetchItem(ctx, data){
        const response = await api.get('/api/v1/items/'+data.id)
        ctx.commit('setSpecificItem', response.data) 
        return response
    },
    async projectFullDetails(ctx, id){
        const response = await api.get('/api/v1/items/'+id)
        .then(response => { 
            ctx.commit('setfullDetailsItem', response.data) 
        })
        .catch(error => {
            console.log(error.response)
        })
        return response
    },
    async serviceFullDetails(ctx, id){
        console.log('wew')
        const response = await api.get('/api/v1/items/'+id)
        .then(response => { 
            ctx.commit('setfullDetailsItem', response.data) 
        })
        .catch(error => {
            console.log(error.response)
        })
        return response
    },
    async productFullDetails(ctx, id){
        const response = await api.get('/api/v1/items/'+id)
        .then(response => { 
            ctx.commit('setfullDetailsItem', response.data) 
        })
        .catch(error => {
            console.log(error.response)
        })
        // ctx.commit('setfullDetailsItem', response.data) 
        // window.location.replace("/#/shopzone/services/full-details")
        return response
    },
    async getProductRating(ctx, id){
        const response = await api.get('/api/v1/item/ratings/'+id)
        // console.log(response.data.results)
        .then(response => { 
            ctx.commit('setProductRating', response.data.results) 
        })
        .catch(error => {
            console.log(error.response)
        })
        return response
    },
    async getProjectBids(ctx, id){
        const response = await api.get('/api/v1/item/bids/'+id)
        // console.log(response.data.results)
        .then(response => { 
            ctx.commit('setProjectBids', response.data.results) 
        })
        .catch(error => {
            console.log(error.response)
        })
        return response
    },
    async postProjectBid(ctx, data){
        const response = await api.post('/api/v1/bids',{
            item_id : data.item_id,
            price    : data.price,
            // comment : data.comment
        })
        .then(response => { 
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response)
        })
        return response
    },
    async deleteBid(ctx, id){
        const response = await api.delete('/api/v1/bids/'+id)
        .then(response => {
            window.location.reload()
          }
        )
        .catch(error => {
            console.log(error)
        })
    },
    async availService(ctx, data){
        const response = await api.post('/api/v1/availed_services',{
            item_id     : data.item_id,
            name        : data.name,
            telephone   : data.telephone,
            description : data.description,
            address     : data.street+', '+data.city+', '+data.region+', '+data.zipcode+', '+data.country,
        })
        .then(response => {
            window.location.reload()
          }
        )
        .catch(error => {
            console.log(error)
        })
    },
}
const MUTATIONS = {
    setProjectList(state, data){
        let arr = [];
        data.data.results.forEach(element => {
                arr.push(element);
        });
        state.list_of_projects = arr
    },
    setServiceList(state, data){
        let arr = [];
        data.data.results.forEach(element => {
                arr.push(element);
        });
        state.list_of_services = arr
    },    
    setProductList(state, data){
        let arr = [];
        data.data.results.forEach(element => {
                arr.push(element);
        });
        state.list_of_product = arr
    },
    setSpecificItem(state, data) {
        state.setSpecificItem.id = data.results.id
        state.setSpecificItem.identifier = data.results.identifier
        state.setSpecificItem.item_description = data.results.item_description
        state.setSpecificItem.item_name = data.results.item_name
        state.setSpecificItem.item_type = data.results.item_type
        state.setSpecificItem.category_id = data.results.category_id
    }, 
    setfullDetailsItem(state, data) {
        state.setFullDetailsItem = data.results
    },
    setProductRating(state, data){
        let arr = [];
        data.item_ratings.forEach(element => {
                arr.push(element);
        });
        state.product_item_ratings = arr
    },
    setProjectBids(state, data){
        let arr = [];
        data.bids.forEach(element => {
                arr.push(element);
        });
        state.project_bids = arr
    } 
}

const GETTERS = {}

const SHOPZONE = {
    namespaced: true,
    state: DEFAULT_STATE(),
    actions: ACTIONS,
    mutations: MUTATIONS,
    getters: GETTERS
}

export default SHOPZONE