module.exports = {
  runtimeCompiler:true,
  publicPath: '/',
  css: {
    extract: false 
  },
  // chainWebpack: webpackConfig => {
  //   if (process.env.NODE_ENV === 'production') {
  //     const inlineLimit = 10000;
  //     const assetsPath = 'static/assets';

  //     webpackConfig.module
  //     .rule('images')
  //     .test(/\.(png|jpe?g|gif)(\?.*)?$/)
  //     .use('url-loader')
  //     .loader('url-loader')
  //     .options({
  //       limit: inlineLimit,
  //       name: path.join(assetsPath, 'images/[name].[hash:8].[ext]')
  //     })
  //   }
  // },

  assetsDir: 'assets',
  devServer: {
    // host: 'localhost',
    proxy: 'http://127.0.0.1:8000'
  },
}

